const express = require('express')
const app = express()

const bodyParser = require('body-parser')
const cors = require('cors')

const fs = require('fs')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.raw({
    type: 'image/jpeg',
    limit: '10mb'
}));

app.use(express.static('public'))

app.get('/captures', (req, res) => {
    res.sendFile(__dirname + "/" + getImageName(req));
});

app.put('/captures', (req, res) => {
    const image = req.body;

    saveImage(image, getImageName(req), res);
});

function getImageName(req) {
    return req.query.token + "-" + req.query.facing + ".jpg" // no facing check
}

function saveImage(image, name, res) {
    if (Buffer.isBuffer(image)) {
        fs.writeFile(name, image, function (err) {
            if (err) {
                res.sendStatus(500); // server error
                throw err;
            }
            res.sendStatus(200); // Ok
        });
    } else {
        res.sendStatus(400);  // bad request
    }
}


const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
